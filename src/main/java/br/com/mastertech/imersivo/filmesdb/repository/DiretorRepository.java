package br.com.mastertech.imersivo.filmesdb.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.mastertech.imersivo.filmesdb.model.Diretor;
import br.com.mastertech.imersivo.filmesdb.model.DiretorDTO;

@Repository
public interface DiretorRepository extends CrudRepository<Diretor, Long>{

	@Query("SELECT d FROM Diretor d WHERE id = :id")
	DiretorDTO findSummary(
			@Param("id") Long id);
	
}
